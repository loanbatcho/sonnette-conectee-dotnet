Based on the detailed descriptions from your personal and group reports, here's a draft for a README file for your GitLab repository. This draft highlights the technical aspects of the "Do Not Disturb" feature you developed, as well as explaining the overall functionality and communication between projects, with a mention of the IIS server as the gateway for these interactions.

---

# Connected Doorbell Solution
This repository contains the Connected Doorbell project, a .NET-based solution that integrates a physical Raspberry Pi-based system with a web application to simulate a smart doorbell mechanism.

## Project Objective
The project aimed to create an integrated system where a physical action (button press) triggers a series of digital events, culminating in a user notification within a chat application.

## System Overview
The system uses a Raspberry Pi as a base for the doorbell, communicating with a web-hosted chat application that serves as a server. This interaction is facilitated using REST APIs, a fundamental component for communication between embedded devices and modern web applications.

## "Do Not Disturb" Feature

### Technical Implementation
The "Do Not Disturb" feature allows users of the SignalRChat application to control the receipt of doorbell notifications.

#### Step 1: Toggle Design
A toggle switch was implemented in the SignalRChat user interface, allowing users to enable or disable the "Do Not Disturb" mode. This triggers a POST request to the SignalRChat server's REST API.

#### Step 2: API Request Handling
The SignalRChat API endpoint (`/DingDong/ToggleChange`) receives the toggle state request and communicates the status to the BlinkTutorialv2 service running on the Raspberry Pi.

#### Step 3: BlinkTutorialv2 Service Interaction
The BlinkTutorialv2 controller processes the request and invokes the `ToggleChange` method of the `MessagingService` class, updating the "Do Not Disturb" status accordingly.

#### Step 4: Raspberry Pi State Update
`MessagingService` updates the static `DoNotDisturb` property, which governs the acceptance of notifications by the embedded system.

#### Step 5: LED Blink Management
When a button press is detected and a notification is to be sent, the connected LED on the Raspberry Pi briefly illuminates for one second. This is followed by a "System says button press" message in the chat application, visible to all chat participants. In any situation where the notification cannot be sent (due to "Do Not Disturb" being enabled or a transmission error like a timeout), the LED blinks rapidly five times, each blink lasting 100 milliseconds, and the cause of the notification rejection is displayed in the MobXterm terminal.

#### Step 6: Role of IIS Server
The IIS server acts as a gateway for all communications between SignalRChat and the Raspberry Pi, enabling a secure and reliable interaction between the two systems.

## Development Challenges
Integrating the two applications and managing the states of the "Do Not Disturb" mode were the primary technical challenges encountered. The functional understanding was the harder part, while the technical side was more manageable with guidance from our professor. Configuring the IIS server for external communication was also a critical point.

## Conclusion
This project was an enriching experience, reinforcing knowledge in .NET technology and REST APIs. Teamwork and brainstorming were valuable aspects of this endeavor. The project pushed the boundaries of our technical knowledge but also expanded it through problem-solving.

---

Please replace `[Figure X]` with the actual paths or inline images as necessary. Also, be sure to include any additional sections or information that might be specific to your GitLab repository setup or organization standards.