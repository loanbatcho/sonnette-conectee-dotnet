﻿using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalRChat.Hubs;
using System.Threading.Tasks;

[ApiController]
public class ButtonController : ControllerBase
{
    private readonly IHubContext<ChatHub> _hubContext;
    private static readonly HttpClient _httpClient = new HttpClient();

    public ButtonController(IHubContext<ChatHub> hubContext)
    {
        _hubContext = hubContext;
    }

    [HttpPost("DingDong")] 
    public async Task<IActionResult> DingDong()
    {
        // Sending a message to all clients that the button was pressed
        await _hubContext.Clients.All.SendAsync("ReceiveMessage", "System", "Button press");
        return Ok("Message sent!");
    }

    [HttpPost("DingDong/ToggleChange")] 
    public async Task<IActionResult> ToggleChange([FromBody] ToggleModel toggle)
    {
        string raspberryPiEndpoint = "http://172.16.227.12:8080/changeStatus"; 

        try
        {
            var response = await _httpClient.PostAsJsonAsync(raspberryPiEndpoint, toggle);

            if (response.IsSuccessStatusCode)
            {
                return Ok();
            }
            else
            {
                // Handle the failure according to your requirements
                return StatusCode((int)response.StatusCode, "Failed to change toggle state."+ response);
            }
        }
        catch (HttpRequestException e)
        {
            // Handle any connectivity exceptions, such as the Raspberry Pi being unreachable
            return StatusCode(503, "Service unavailable: " + e.Message);
        }
    }
}

public class ToggleModel
{
    public bool DoNotDisturb { get; set; }
}
