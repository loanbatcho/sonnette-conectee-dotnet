﻿"use strict";
var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

//Disable the send button until connection is established.
document.getElementById("sendButton").disabled = true;

connection.on("ReceiveMessage", function (user, message) {
    var li = document.createElement("li");
    document.getElementById("messagesList").appendChild(li);
    // We can assign user-supplied strings to an element's textContent because it
    // is not interpreted as markup. If you're assigning in any other way, you 
    // should be aware of possible script injection concerns.
    li.textContent = `${user} says ${message}`;
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("dndToggle").addEventListener("change", async function (event) {
    const isDndEnabled = event.target.checked;
    try {
        const response = await toggleDoNotDisturb(isDndEnabled);
        // Additional actions based on the response can go here
    } catch (error) {
        // Handle any errors from the API call
        console.log('There was an issue toggling DND:', error);
    }

    event.preventDefault(); // Prevent the default action
});
document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

async function toggleDoNotDisturb(isDndEnabled) {
    const apiUrl = window.location.origin +"/DingDong/ToggleChange";
    console.log(apiUrl);
    try {
        const response = await fetch(apiUrl, {
            method: 'POST',
            body: JSON.stringify({ doNotDisturb: isDndEnabled }),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Network response was not ok: ' + response.statusText);
        }

        const data = await response;
        console.log('Toggle DND status response:', data);
        return data; // Return the response data for further processing if necessary
    } catch (error) {
        console.error('Error when toggling DND status:', error);
        throw error; // Re-throw the error for the caller to handle
    }
}