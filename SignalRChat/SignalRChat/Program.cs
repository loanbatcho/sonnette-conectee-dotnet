using SignalRChat.Hubs;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddControllers();
builder.Services.AddSignalR();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
else
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        // Make sure to include the Swagger JSON endpoint.
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "My SignalRChat API V1");
        c.RoutePrefix = string.Empty; // to serve the Swagger UI at the app's root (http://localhost:<port>/)
    });
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.MapControllers();
app.MapRazorPages();
app.MapHub<ChatHub>("/chatHub");
app.MapSwagger();
app.UseCors(policy =>
{
    policy.WithOrigins("http://172.16.227.23:82/")
          .AllowAnyHeader()
          .AllowAnyMethod()
          .AllowCredentials();
});

app.Run();
