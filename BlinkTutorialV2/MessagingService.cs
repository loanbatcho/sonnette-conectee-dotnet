﻿using System;
using System.Device.Gpio;
using System.Net.Http;
using System.Threading.Tasks;

public class MessagingService
{
    private static readonly GpioController _controller = new GpioController();
    private static readonly HttpClient _client = new HttpClient();
    private const int LedPin = 18;
    private const int ButtonPin = 17;
    private readonly string _apiUrl = "http://172.16.227.23:82/DingDong";
    public bool DoNotDisturb { get; set; } = false;

    public MessagingService()
    {
        _controller.OpenPin(LedPin, PinMode.Output);
        _controller.OpenPin(ButtonPin, PinMode.InputPullUp);

        _controller.RegisterCallbackForPinValueChangedEvent(
            ButtonPin,
            PinEventTypes.Falling,
            async (sender, args) => await HandleButtonPressAsync());
    }

    private async Task HandleButtonPressAsync()
    {
        if (DoNotDisturb)
        {
            Console.WriteLine("Do Not Disturb is enabled. Message will not be sent.");
            BlinkLed(5, 200);
        }
        else
        {
            await SendMessageAsync();
        }
    }

    private async Task SendMessageAsync()
    {
        try
        {
            var response = await _client.PostAsync(_apiUrl, null);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Message successfully sent ✔");
                BlinkLed(1, 1000);
            }
            else
            {
                Console.WriteLine("Failed to send message.");
                Console.WriteLine($"API Response: {response.StatusCode}");
                BlinkLed(5, 100);
            }
        }
        catch (TaskCanceledException ex) when (ex.InnerException is TimeoutException)
        {
            Console.WriteLine("A timeout occurred while sending the API request.");
            BlinkLed(10, 100);
        }
        catch (HttpRequestException ex)
        {
            Console.WriteLine("A connection error occurred.");
            BlinkLed(10, 100);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
            BlinkLed(10, 100);
        }
    }

    private void BlinkLed(int blinks, int duration)
    {
        Console.WriteLine($"Blinking LED {blinks} time(s).");
        for (int i = 0; i < blinks; i++)
        {
            _controller.Write(LedPin, PinValue.High);
            Task.Delay(duration).Wait();
            _controller.Write(LedPin, PinValue.Low);
            if (i < blinks - 1)
            {
                Task.Delay(duration).Wait();
            }
        }
    }
}
