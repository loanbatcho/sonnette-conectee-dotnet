﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
[ApiController]
public class DoNotDisturbController : ControllerBase
{
    private readonly MessagingService _messagingService;

    public DoNotDisturbController(MessagingService messagingService)
    {
        _messagingService = messagingService;
    }

    [HttpGet("getStatus")]
    public IActionResult GetStatus()
    {
        return Ok(_messagingService.DoNotDisturb);
    }

    [HttpPost("changeStatus")]
    public IActionResult SetStatus([FromBody] ToggleModel toggle)
    {
        _messagingService.DoNotDisturb = toggle.DoNotDisturb;
        return Ok();
    }
}

public class ToggleModel
{
    public bool DoNotDisturb { get; set; }
}
