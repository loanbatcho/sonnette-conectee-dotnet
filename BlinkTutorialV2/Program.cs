
var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

builder.Services.AddSingleton<MessagingService>();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var app = builder.Build();
var messagingService = app.Services.GetRequiredService<MessagingService>();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

/*
app.UseHttpsRedirection();*/
app.UseAuthorization();
app.MapControllers();
app.Run();


